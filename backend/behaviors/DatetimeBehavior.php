<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 10.05.18
 * Time: 17:51
 */

namespace app\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Expression;


class DatetimeBehavior extends Behavior
{
    private $createdAt ;
    private $updatedAt ;


    public function init()
    {
        parent::init();
    }

    public function  getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($time)
    {
        $this->createdAt = $time;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($time)
    {
        $this->updatedAt = $time;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ];
    }

    public function beforeInsert()
    {
        $this->owner->updateAttributes(array_fill_keys([$this->createdAt,$this->updatedAt], new Expression('NOW()')));
    }

    public function beforeUpdate()
    {
        $this->owner->updateAttributes(array_fill_keys((array) $this->updatedAt, new Expression('NOW()')));
    }

}