<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<?
echo \yii\grid\GridView::widget([
    'dataProvider' => $data,
    'filterModel' => $search,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //'id',
        'category.name',
        'uploadForm.url',
        'name:ntext',
        /*[
            'attribute'=>'nameProducts',
            'format' => 'text',
            'content'=>function($data){
                return $data['name'];
            }
        ],*/
        'created_at',
        'updated_at',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]);
?>

<?= Html::a('Add', ['/task/update'], ['class'=>'btn btn-primary']) ?>

