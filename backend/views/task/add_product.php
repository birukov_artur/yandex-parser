<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 10.05.18
 * Time: 14:49
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div>

</div>
<div class="row">

        <div class="col-lg-5">
            <?= Html::img(    Url::to($img), ['width' => 400]) ?>
            <!--<?= Html::a('Add image', ['/task/upload', "id" => $model['id']], ['class'=>'btn btn-primary']) ?>-->

            <?php $form = ActiveForm::begin(['id' => 'login-form','options' => ['enctype' => 'multipart/form-data']]); ?>

                <?= $this->render('upload_product_img',['model'=>$model,'form'=>$form]);  ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'category_id')->dropDownList($cat, ['prompt'=>'Select Category']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Add', ['class' => 'btn btn-primary', 'name' => 'add-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>