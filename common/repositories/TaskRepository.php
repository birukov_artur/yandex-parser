<?php


namespace common\repositories;


use common\models\Task;

class TaskRepository
{
    /**
     * @param int $id
     * @return Task|null
     */
    public function get(int $id): ?Task
    {
        return Task::findOne(['id' => $id]);
    }
}