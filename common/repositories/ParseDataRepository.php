<?php
namespace common\repositories;


use common\models\ParseData;
use common\models\SearchQuery;
use common\models\Site;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

class ParseDataRepository
{

    /**
     * @param int $queryId
     * @param int $regionId
     * @return array
     */
    public function getForQuery(int $queryId, int $regionId): array
    {
        return ParseData::find()
            ->select(['position', 'url', 'query', 'created_at'])
            ->leftJoin('site', 'site.id = yandex_parse_data.site_id')
            ->leftJoin('query', 'query.id = yandex_parse_data.query_id')
            ->where(['region_id' => $regionId, 'query.id' => $queryId])
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()
            ->all();
    }

    /**
     * @param int $taskId
     * @return array
     */
    public function getForTask(int $taskId): array
    {
        return ParseData::find()
            ->select(['position', 'url', 'query', 'yandex_parse_data.created_at'])
            ->leftJoin('site', 'site.id = yandex_parse_data.site_id')
            ->leftJoin('query', 'query.id = yandex_parse_data.query_id')
            ->where(['task_id' => $taskId])
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()
            ->all();
    }

    /**
     * @return array
     */
    public function getAllQueries(): array
    {
        return SearchQuery::find()->asArray()->all();
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        return Site::find()
            ->select(['id'])
            ->asArray()
            ->indexBy('url')
            ->column();
    }

    /**
     * @param string $query
     * @return SearchQuery|array|ActiveRecord|null
     */
    public function findQuery(string $query)
    {
        return SearchQuery::find()
            ->where(['query' => $query])
            ->one();
    }

    /**
     * @param array $rows
     * @throws Exception
     */
    public function insertUrls(array $rows)
    {
        Yii::$app->db->createCommand()->batchInsert(Site::tableName(), ['url'], $rows)->execute();
    }

    /**
     * @param array $rows
     * @throws Exception
     */
    public function  insertPositions(array $rows)
    {
        $fields = ['site_id', 'query_id', 'position', 'region_id', 'task_id', 'created_at'];
        Yii::$app->db->createCommand()->batchInsert(ParseData::tableName(), $fields, $rows)->execute();
    }
}