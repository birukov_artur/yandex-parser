<?php


namespace common\models;


use yii\db\ActiveRecord;

/**
 * @property string query
 */
class SearchQuery extends ActiveRecord
{
    public static function tableName()
    {
        return 'query';
    }

    public function rules()
    {
        return [
            [['query'], 'required'],
            [['query'], 'string'],
        ];
    }
}