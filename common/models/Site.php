<?php


namespace common\models;


use yii\db\ActiveRecord;

class Site extends ActiveRecord
{
    public static function tableName()
    {
        return 'site';
    }

    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'string'],
        ];
    }

}