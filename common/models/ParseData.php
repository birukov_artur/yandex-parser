<?php


namespace common\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class ParseData extends ActiveRecord
{
    public static function tableName()
    {
        return 'yandex_parse_data';
    }

    public function rules()
    {
        return [
            [['site_id', 'query_id', 'position', 'region_id'], 'required'],
            [['site_id', 'query_id', 'position', 'region_id', 'task_id'], 'integer'],
        ];
    }
}