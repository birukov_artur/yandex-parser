<?php


namespace common\models;


use yii\db\ActiveRecord;

class Task extends ActiveRecord
{
    public static function tableName()
    {
        return 'process';
    }

    public function rules()
    {
        return [
            [['pid', 'query_id', 'region_id'], 'integer'],
            ['is_terminated', 'boolean']
        ];
    }
}