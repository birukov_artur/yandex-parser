<?php


namespace common\models;


use common\repositories\ParseDataRepository;
use yii\db\Exception;

class ParseDataModel
{

    private $repository;

    private $allUrls;
    private $siteMap;
    private $query;
    private $regionId;
    private $pageIndex;
    private $taskId;
    private $time;

    public function __construct(ParseDataRepository $repository)
    {
        $this->repository = $repository;
        $this->time = date("Y-m-d H:i:s");
    }

    /**
     * @param array $siteMap
     * @param string $query
     * @param int $regionId
     * @param int $pageIndex
     * @param int $taskId
     * @throws Exception
     */
    public function process(array $siteMap, string $query, int $regionId, int $pageIndex, int $taskId): void
    {
        $this->siteMap = $siteMap;
        $this->updateCurrentUrls();
        $this->regionId = $regionId;
        $this->pageIndex = $pageIndex;
        $this->taskId = $taskId;
        $this->query = $this->repository->findQuery($query);
        $this->addUrls(array_keys($siteMap));
        $this->addPositions();
    }

    /**
     * @param $urls
     * @throws Exception
     */
    private function addUrls($urls): void
    {
        $urlsToAdd = [];

        foreach ($urls as $url){
            if(!isset($this->allUrls[$url])){
                $urlsToAdd[] = ['url' => $url];
            }
        }

        $this->repository->insertUrls($urlsToAdd);
        $this->updateCurrentUrls();
    }

    private function addPositions(): void
    {
        $parseDataToAdd = [];

        foreach ($this->siteMap as $url => $positions){
            foreach ($positions as $position){
                $parseDataToAdd[] = [
                    'site_id' => $this->allUrls[$url],
                    'query_id' => $this->query->id,
                    'position' => $position + $this->pageIndex * 10,
                    'region_id' => $this->regionId,
                    'task_id' => $this->taskId,
                    'created_at' => $this->time,
                ];
            }
        }

        $this->repository->insertPositions($parseDataToAdd);
    }

    private function updateCurrentUrls(): void
    {
        $this->allUrls = $this->repository->getUrls();
    }
}