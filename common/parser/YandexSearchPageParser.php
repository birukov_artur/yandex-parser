<?php


namespace common\parser;


use Exception;
use simplehtmldom_1_5\simple_html_dom_node;
use Sunra\PhpSimple\HtmlDomParser;

class YandexSearchPageParser
{
    private $page;

    /**
     * YandexSearchPageParser constructor.
     * @param string $html
     */
    public function __construct(string $html)
    {
        $this->setPage($html);
    }

    /**
     * @param string $html
     */
    public function setPage(string $html)
    {
        $this->page = HtmlDomParser::str_get_html($html);
    }

    public function getSiteMap(): array
    {
        $siteMap = [];

        $searchResultElementsList = $this->page->find('.serp-item');
        $position = 0;
        foreach ($searchResultElementsList as $searchResultEl) {

            if($this->hasChildElement($searchResultEl, 'organic__label')){
                continue;
            }

            $urlElement = $searchResultEl->find('.organic__path b')[0] ?? null;
            $url = $urlElement ? $urlElement->text() : null;
            if (!$url || strpos($url, 'Яндекс') === 0) {
                continue;
            }

            $siteMap[$url][] = ++$position;
        }

        return $siteMap;
    }


    /**
     * @param simple_html_dom_node $sourceEl
     * @param string $searchClass
     * @return bool
     */
    private function hasChildElement(simple_html_dom_node $sourceEl, string $searchClass):bool
    {
        $currentClass = $sourceEl->attr['class'] ?? null;

        $classes = explode(' ', $currentClass);
        foreach ($classes as $class){
            if($class == $searchClass){
                return true;
            }
        }

        foreach ($sourceEl->children as $child){
            if($this->hasChildElement($child, $searchClass)){
                return true;
            }
        }

        return false;
    }


    /**
     * @return mixed
     * @throws Exception
     */
    public function getCaptchaImageSource()
    {
        $images = $this->page->find('img.form__captcha');
        $image = $images[0] ?? null;

        if (!$image) {
            return null;
        }

        return $image->getAttribute('src');
    }

    /**
     * @param string $captcha
     * @return string
     */
    public function getUrlForReCaptcha(string $captcha): string
    {
        $inputData = $this->page->find('.form__inner input[name="key"]');
        $repathData = $this->page->find('.form__inner input[name="retpath"]');
        $keyInput = $inputData[0] ?? null;
        $repathInput = $repathData[0] ?? null;
        $url = 'https://yandex.ru/checkcaptcha?rep=' . urlencode($captcha);
        if ($keyInput) {
            $url .= '&key=' . $keyInput->getAttribute('value');
        }
        if ($repathInput) {
            $url .= '&retpath=' . urlencode(html_entity_decode($repathInput->getAttribute('value')));
        }

        return $url;
    }
}