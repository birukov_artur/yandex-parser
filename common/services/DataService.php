<?php


namespace common\services;


use common\models\SearchQuery;
use common\repositories\ParseDataRepository;

class DataService
{
    private $repository;


    public function __construct(ParseDataRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $queryId
     * @param int $regionId
     * @return array
     */
    public function getForQuery(int $queryId, int $regionId): array
    {
        return $this->repository->getForQuery($queryId, $regionId);
    }

    /**
     * @param $taskId
     * @return array
     */
    public function getForTask(int $taskId): array
    {
        return $this->repository->getForTask($taskId);
    }

    /**
     * @return array
     */
    public function getAllQueries(): array
    {
        return $this->repository->getAllQueries();
    }

    /**
     * @param string $query
     * @return int
     */
    public function addQuery(string $query): int
    {
        $queryModel = $this->repository->findQuery($query);
        if(!$queryModel) {
            $queryModel = new SearchQuery();
            $queryModel->query = $query;
            $queryModel->save();
        }

        return $queryModel->id ?? null;
    }
}