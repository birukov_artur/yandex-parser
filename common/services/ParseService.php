<?php

namespace common\services;

use common\helpers\RequestHelper;
use common\models\ParseDataModel;
use common\parser\YandexSearchPageParser;
use GuzzleHttp\Exception\GuzzleException;
use Yii;
use yii\base\InvalidConfigException;

class ParseService
{
    /**
     * @param string $query
     * @param int $regionId
     * @param int $taskId
     * @param int $depth
     * @throws GuzzleException
     * @throws InvalidConfigException
     * @throws \Exception
     */
    public function parseYandexSearchResults(string $query, int $regionId, int $depth, int $taskId)
    {
        $parseDataModel =  Yii::createObject(ParseDataModel::class);

        for($pageIndex = 0; $pageIndex < $depth; $pageIndex++){

            $url = 'https://yandex.ru/search';
            $params = ['text' => $query, 'lr' => $regionId, 'p' => $pageIndex];
            $htmlSource = RequestHelper::sendGetRequest($url, $params);
            $htmlSource .= '</body></html>';
            $parser = new YandexSearchPageParser($htmlSource);

            $captchaSource = $parser->getCaptchaImageSource();
            while($captchaSource){
                $captchaSolution = $this->getCaptchaSolution($captchaSource);
                $reCaptchaUrl = $parser->getUrlForReCaptcha($captchaSolution);
                $htmlSource = RequestHelper::sendGetRequest($reCaptchaUrl);
                $htmlSource .= '</body></html>';
                $parser->setPage($htmlSource);
                $captchaSource = $parser->getCaptchaImageSource();
            }

            $siteMap = $parser->getSiteMap();

            if(empty($siteMap)){
                throw new \Exception('Неверно разгаданная капча');
            }

            $transaction = \Yii::$app->db->beginTransaction();

            try{
                $parseDataModel->process($siteMap, $query, $regionId, $pageIndex, $taskId);

                $transaction->commit();
            } catch (\Throwable $e){
                $transaction->rollBack();
                throw new \Exception('Ошибка при записи данных в базу');
            }
        }
    }

    /**
     * @param string $captchaSource
     * @return mixed
     * @throws \Exception
     */
    public function getCaptchaSolution(string $captchaSource)
    {
        sleep(1);
        if (!Yii::$app->captcha->run($captchaSource)) {
            throw new \Exception(Yii::$app->captcha->error());
        }

        return Yii::$app->captcha->result();
    }
}