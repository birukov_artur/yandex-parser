<?php


namespace common\services;


use common\models\Task;
use common\repositories\ParseDataRepository;
use common\repositories\TaskRepository;
use Exception;
use frontend\models\ParserForm;

class TaskService
{

    private $repository;
    private $parseDataRepository;

    public function __construct(TaskRepository $repository, ParseDataRepository $parseDataRepository)
    {
        $this->repository = $repository;
        $this->parseDataRepository = $parseDataRepository;
    }

    /**
     * @param ParserForm $model
     * @param int $taskId
     * @return void
     * @throws Exception
     */
    public function runParser(ParserForm $model, int $taskId): void
    {
        $dir = \Yii::getAlias('@root');
        $process = new \Symfony\Component\Process\Process(['/opt/php71/bin/php', 'yii', 'parse/parse', $model->query, $model->regionId, $model->depth, $taskId], $dir);

        session_write_close();
        $process->setTimeout(3600);
        $process->run();
        $this->updateProcessStatus($taskId,true);

        if (!$process->isSuccessful()) {
            throw new Exception($process->getErrorOutput());
        }
    }

    /**
     * @param $id
     * @return Task|null
     */
    public function get(int $id): ?Task
    {
        return $this->repository->get($id);
    }

    /**
     * @param int $queryId
     * @param int $regionId
     * @return int|null
     */
    public function create(int $queryId, int $regionId): ?int
    {
        $task = new Task();

        $task->query_id = $queryId;
        $task->region_id = $regionId;
        $task->is_terminated = false;

        return  $task->save() ? $task->id : null;
    }

    /**
     * @param int $taskId
     * @param bool $isTerminated
     */
    public function updateProcessStatus(int $taskId, bool $isTerminated): void
    {
        $task = $this->repository->get($taskId);

        if($task){
            $task->is_terminated = $isTerminated;
            $task->save();
        }
    }

    /**
     * @param int $taskId
     * @param int $pid
     */
    public function updatePid(int $taskId, int $pid): void
    {
        $task = $this->repository->get($taskId);

        if($task){
            $task->pid = $pid;
            $task->save();
        }
    }
}