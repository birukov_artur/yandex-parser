<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'captcha' => [
            'domain' => 'anti-captcha.com',
            'class' => 'jumper423\Captcha',
            'pathTmp' => '@app/files/captcha',
            'apiKey' => '279060ca497f713d01d6f0adeb38cab8'
        ],
    ],
];
