<?php

namespace common\helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Yii;
use yii\base\InvalidConfigException;

class RequestHelper
{
    /**
     * @param string $url
     * @param array $queryParams
     * @param int $timeout
     * @return string
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public static function sendGetRequest(string $url, array $queryParams = [], int $timeout = 0)
    {
        $client = Yii::createObject(Client::class);
        $jar = new CookieJar;

        $params = ['cookies' => $jar];
        if($timeout){
            $params['timeout'] = $timeout;
        }
        if($queryParams){
            $params['query'] = $queryParams;
        }

        $response = $client->request('GET', $url, $params);
        $html = $response->getBody()->getContents();

        return $html;
    }
}