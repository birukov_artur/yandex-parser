<?php
namespace console\controllers;

use common\services\ParseService;
use GuzzleHttp\Exception\GuzzleException;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;

class ParseController extends Controller
{

    /**
     * @param $query
     * @param $regionId
     * @param $depth
     * @param $taskId
     * @throws GuzzleException
     * @throws InvalidConfigException
     * @throws \Exception
     */
    public function actionParse($query, $regionId, $depth, $taskId)
    {
        $service = Yii::createObject(ParseService::class);
        $service->parseYandexSearchResults($query, $regionId, $depth, $taskId);
    }
}