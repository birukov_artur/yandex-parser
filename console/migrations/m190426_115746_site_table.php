<?php

use yii\db\Migration;

/**
 * Class m190426_115746_site_table
 */
class m190426_115746_site_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('site', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
        ]);

        $this->createTable('query', [
            'id' => $this->primaryKey(),
            'query' => $this->string()->notNull(),
        ]);

        $this->createTable('yandex_parse_data', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer(),
            'query_id' => $this->integer(),
            'position' =>  $this->integer(),
            'region_id' => $this->integer(),
            'created_at' => $this->timestamp(),
        ]);

        $this->addForeignKey(
            'fk-yandex_parse_data-site_id',
            'yandex_parse_data',
            'site_id',
            'site',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-yandex_parse_data-query_id',
            'yandex_parse_data',
            'query_id',
            'query',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-yandex_parse_data-site_id','yandex_parse_data');
        $this->dropForeignKey('fk-yandex_parse_data-query_id','yandex_parse_data');
        $this->dropTable('site');
        $this->dropTable('query');
        $this->dropTable('yandex_parse_data');
    }
}
