<?php

use yii\db\Migration;

/**
 * Class m190520_080949_process_table
 */
class m190520_080949_process_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('process', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer(),
            'query_id' => $this->integer(),
            'region_id' => $this->integer(),
            'is_terminated' => $this->boolean(),
            'created_at' => $this->timestamp(),
        ]);

        $this->addForeignKey(
            'fk-process-query_id',
            'process',
            'query_id',
            'query',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-process-query_id','process');
        $this->dropTable('process');
    }
}
