<?php

use yii\db\Migration;

/**
 * Class m190522_133100_add_task_id_to_parse_data
 */
class m190522_133100_add_task_id_to_parse_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('yandex_parse_data', 'task_id', $this->integer()->after('region_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('yandex_parse_data', 'task_id');
    }
}
