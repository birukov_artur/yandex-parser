var timer;

$("#parser-form").submit(function(e) {

    e.preventDefault();
    e.stopImmediatePropagation();

    var $form = $(this);
    var data = $form.serializeArray();

    $.ajax({
            type: 'post',
            url: '/site/create-task',
            data: data,
            dataType: "json",
            success: function (response) {
                if(response.success){
                    var taskId = response.result;
                    $('#task-data').data('id', taskId);
                    $('.parse-loading').show();
                    parseRequest(taskId, data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });

    return false;
});

$("#result-form").submit(function() {
    var $form = $(this);
    renderTable($form.serializeArray());

    return false;
});

function checkTask() {

    var taskId = $('#task-data').data('id');

    if(!taskId){
        return;
    }
    renderTableForTask(taskId);

    $.ajax({
            url: '/site/check-task',
            type: 'get',
            dataType: 'json',
            data: {
                taskId: taskId
            },
            success: function (response) {
                if(response.success && response.result){
                    console.log('done');
                    clearInterval(timer);
                    $('.parse-loading').hide();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
    });
}

function renderTableForTask(taskId) {
    $.ajax({
        type: 'get',
        url: '/site/render-table-for-task',
        data: {taskId: taskId},
        dataType: "html",
        success: function (response) {
            $('#result-table').html(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function renderTable(data) {
    $.ajax({
        type: 'post',
        url: '/site/render-table',
        data: data,
        dataType: "html",
        success: function (response) {
            $('#result-table').html(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function parseRequest(taskId, params)
{
    var data =  params;
    data.push({name: 'taskId', value: taskId});
    setTimer();

    $.ajax({
        type: 'post',
        url: '/site/parse',
        data: data,
        dataType: "json",
        success: function (response) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function setTimer(){
    timer = setInterval(checkTask, 5000);
}
