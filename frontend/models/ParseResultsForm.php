<?php


namespace frontend\models;


use yii\base\Model;

class ParseResultsForm extends Model
{
    public $queryId;
    public $regionId;

    public function rules()
    {
        return [
            [['queryId', 'regionId'], 'required'],
            [['regionId', 'queryId'], 'integer'],
        ];
    }
}