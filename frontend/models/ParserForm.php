<?php


namespace frontend\models;


use yii\base\Model;

class ParserForm extends Model
{
    public $query;
    public $depth;
    public $regionId;

    public function rules()
    {
        return [
            [['query', 'depth', 'regionId'], 'required'],
            [['depth', 'regionId'], 'integer'],
            [['query'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'query' => 'Поисковый запрос',
            'depth' => 'Количество страниц для парсинга',
            'regionId' => 'ID региона',
        ];
    }
}