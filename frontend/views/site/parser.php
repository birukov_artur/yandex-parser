<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php

$model = new \frontend\models\ParserForm();

$form = ActiveForm::begin([
    'id'     => 'parser-form',
    'action' => ['/site/create-task'],
    'method' => 'post',
]);
?>

<?= $form->field($model, 'query')->textInput([
    'autocomplete' => 'off',
    'placeholder'  => 'Поисковый запрос',
])->label(false) ?>

<?= $form->field($model, 'regionId')->textInput([
    'autocomplete' => 'off',
    'placeholder'  => 'ID региона',
])->label(false) ?>

<?= $form->field($model, 'depth')->textInput([
    'autocomplete' => 'off',
    'placeholder'  => 'Количество страниц для парсинга',
])->label(false) ?>

<?= Html::submitButton('Go') ?>

<?php ActiveForm::end(); ?>

<br><br>

<div class="parse-loading"></div>
<div id="result-table"></div>
<div id="task-data" data-id=""></div>
