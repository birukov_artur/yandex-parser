<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>

<div id="task-data" data-id="<?= $task->id ?>" data-regionId="<?= $task->region_id ?>" data-queryId="<?= $task->query_id?>"></div>

<div>Запрос: <?= $query ?></div>


<?
$model = new \frontend\models\ParseResultsForm();

$form = ActiveForm::begin([
    'id'     => 'result-form',
    'action' => ['/site/render-table'],
    'method' => 'post',
    'enableAjaxValidation' => true,
]);
?>

<div>
    <?= $form->field($model, 'queryId')->dropDownList([$task->query_id => $query],['disabled' => 'disabled'])
        ->label(false); ?>
    <?= $form->field($model, 'regionId')->textInput([
            'value' => $task->region_id   ,
        'placeholder'  => 'ID региона',
    ])->label(false); ?>
</div>

<?= Html::submitButton('Показать результаты') ?>

<?php ActiveForm::end(); ?>


<br><br>

<div id="result-table"></div>







