<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>


<?

$model = new \frontend\models\ParseResultsForm();

$form = ActiveForm::begin([
    'id'     => 'result-form',
    'action' => ['/site/render-table'],
    'method' => 'post',
    'enableAjaxValidation' => true,
]);
?>

<div>
    <?= $form->field($model, 'queryId')->dropDownList(['' => 'Выберите запрос'] + $queries)
        ->label(false); ?>
    <?= $form->field($model, 'regionId')->textInput([
        'placeholder'  => 'ID региона',
    ])->label(false); ?>
</div>

<?= Html::submitButton('Показать результаты') ?>

<?php ActiveForm::end(); ?>

<br><br>

<div id="result-table"></div>


