<?php
?>

<? if(!empty($data)):?>
<table>
    <tr>
        <th>Позиция</th>
        <? foreach($dates as $date): ?>
        <th><?= $date ?></th>
        <? endforeach;?>
    </tr>

    <? foreach($data as $position => $sites):?>
    <tr>
        <td><?= $position ?></td>
        <? foreach($dates as $date): ?>
            <td>
                <? $url = $sites[$date]['url'] ?? null; ?>
                <? if($url):?>
                    <a href="<?= 'https://' . $url ?>"><?= $url ?></a>
                <? endif;?>
            </td>
        <? endforeach;?>
    </tr>
    <? endforeach;?>
</table>
<? endif;?>

<style>
    table, th, td {
        border: 1px solid black;
    }

    tr:nth-child(even) {background-color: #f2f2f2;}

    th {
        background-color: #4CAF50;
        color: white;
    }

    td {
        height: 50px;
        vertical-align: bottom;
    }

    th, td {
        padding: 15px;
        text-align: left;
    }
</style>