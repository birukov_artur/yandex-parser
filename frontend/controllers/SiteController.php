<?php
namespace frontend\controllers;

use common\services\DataService;
use common\services\ParseService;
use common\services\TaskService;
use frontend\models\ParseResultsForm;
use frontend\models\ParserForm;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\SignupForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    private $taskService;
    private $dataService;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(string $id, Module $module, array $config = [])
    {
        $this->taskService = Yii::createObject(TaskService::class);
        $this->dataService = Yii::createObject(DataService::class);

        parent::__construct($id, $module, $config);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('parser');
    }

    public function actionCreateTask()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();

        $model = new ParserForm();
        if ($model->load($post) && $model->validate()) {
            $queryId = $this->dataService->addQuery($model->query);
            if($queryId) {
                $taskId = $this->taskService->create($queryId, $model->regionId);
            }
        }

        return ['success' => isset($taskId), 'result' => $taskId ?? null];
    }

    /**
     * @throws \Exception
     */
    public function actionParse()
    {
        $post = Yii::$app->request->post();

        $model = new ParserForm();
        if ($model->load($post) && $model->validate()) {
            $taskId = (int)$post['taskId'];
            $this->taskService->runParser($model, $taskId);
        }
    }

    public function actionTask($taskId, $query)
    {
        $task = $this->taskService->get($taskId);

        return $this->render('task', [
            'query' => $query,
            'task' => $task,
        ]);
    }

    public function actionCheckTask($taskId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $task = $this->taskService->get((int)$taskId);

        return ['success' => isset($task), 'result' => $task->is_terminated ?? false];
    }

    /**
     * @return string
     */
    public function actionResults()
    {
        $queries = $this->dataService->getAllQueries();
        $queries = ArrayHelper::map($queries, 'id', 'query');

        return $this->render('results', [
            'queries' => $queries,
        ]);
    }

    public function actionRenderTable()
    {
        $post = Yii::$app->request->post();
        $model = new ParseResultsForm();
        if ($model->load($post) && $model->validate()) {
            $data = $this->dataService->getForQuery($model->queryId, $model->regionId);
            $html = $this->getTable($data);
        }

        return $html ?? '';
    }

    public function actionRenderTableForTask($taskId)
    {
        $data = $this->dataService->getForTask((int)$taskId);

        return $this->getTable($data);
    }

    private function getTable(array $data)
    {
        $dates = array_unique(array_column($data, 'created_at'));
        $data = ArrayHelper::index($data, 'created_at','position');
        ksort($data);

        return $this->renderPartial('table', [
            'dates' => $dates,
            'data' => $data,
        ]);
    }

    public function actionFlushCache()
    {
        Yii::$app->cache->flush();
    }

    public function actionTest()
    {
        $service = Yii::createObject(ParseService::class);
        $service->parseYandexSearchResults('php', 38, 5, 212);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
